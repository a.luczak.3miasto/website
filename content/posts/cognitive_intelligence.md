# Cognitive intelligence for PM10
## Decision Support system for the implementation of EU PM10 regulations based on the PM10 open governmental data driven by integrated cognitive intelligence

The aim of this project is to develop a system for the support of the implementation on the EU regulations based on the PM10 open governmental data driven by integrated cognitive intelligence for PM10 dust hazards. Our research shows how important it is to adapt the EU directives by individual countries to the development of Smart Cities and the measurement of the PM10 open governmental data. The analysis of the current regulations/directives also indicates how many of those documents are of a recommendation character. The countries which do not follow such recommendation do not face any consequences for the lack of implementation of the above-mentioned regulations. On the other hand, however, too restrictive documents are too difficult to be implemented.

The examples of the EU regulations (as recommendations) are:

* the INSPIRE directive (Infrastructure for Spatial Information in Europe) which determines the infrastructure of spatial information in Europe. Its aim is to support environmental protection policies of the community and any policies or activities which may affect the environment,
* No. 96/62/WE of 27th September 1996 on the assessment and management of air quality,
* No. 1999/30/WE of 22nd April 1999 on the limit values of sulphur dioxide, nitrogen dioxide and nitrogen oxide, dust and lead in ambient air,
* No. 2000/69/WE of 16th November 2000 on the limit values of benzene and carbon monoxide in ambient air,
* No. 2002/3/WE of 12th February 2002 on ozone in ambient air,

