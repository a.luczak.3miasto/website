# IBM Watson Build - 2017
## IBM Watson Tradeoff Analytics for decision support design of hybrid measurement systems (IoT sensors and reference stations for PM10) for Coastal areas

The aim of the proposal is to utilize IBM Watson for the assessment of ambient air quality for Smart Cities. The proposal is related to the problem of design, development and integration of environmental micro sensors and their networks for the support of Smart Cities. The project addresses research problems in the field of chemical and environmental sciences, through the following:

* assessment of the quality of the measurement data with the aid of appropriately developed methodologies;
* development of new Computational Intelligence-based tools for the improvement of environmental micro sensor performance for optimizing urban metabolism in the context of Smart Cities,
* design and use of Internet of Things (IoT) measurement devices and assessment of their usability,
* analysis of possibilities referring to the design of hybrid networks (IoT plus reference devices) with the use of cloud solutions,
development of environmental monitoring standards for Smart Cities in terms of the ambient air quality assessment,
* provision of quality of life-related information (environmental alerting, everyday utility support, etc) with the aid of cognitive technologies.

The resulting project product will support the following business scenarios:

1. environmental micro sensor network design, setup, optimization and use in urban areas,
2. environmental data collection and processing, 
3. forecasting and user tailored intelligence provision,
4. citizen-centered quality of life information service generation.
