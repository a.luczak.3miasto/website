# Method and system for designing hybrid networks for air quality measurement using neural networks

This is a proposal for a patent application. Therefore, the description of the method proposed in the application is shortened. The design method proposed in the application meets the expectations of citizens and city decision makers to design the Internet of Things measuring networks. The use of the method of designing hybrid measurement networks proposed in the application will significantly improve and facilitate the process of designing networks for measuring air quality, and as a consequence improve the decision making process regarding the investment of cities in such measurement networks.

The application is ready to be filed with a patent attorney.

