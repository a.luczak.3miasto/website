# Karteco-Software Development Environment
In the period from May 2019 to September 2019, the Jazzops team implemented a project for Karteco (Greece) regarding the construction of an IoT system manufacturing environment. This project was funded by the Erasmus program under which the Jazzopps team had a three-month industrial practice in Thessaloniki. As part of this industrial practice, the team learned about the requirements of the Karteco development team regarding the design of IT systems for environmental protection. Then, based on these requirements, the architecture of the manufacturing environment was constructed for the Karteco team. This architecture was based on flow systems under which services are designed and then included in the integration bus. Photograph 1 presents the meeting initiating the project (jazzops design team and Karteco employees).

![Karteco](https://gitlab.com/airq/website/-/blob/main/Images/Karteco_3.jpeg "Karteco1")


Picture 2 presents the meeting summarizing the project under which the Karteco team received a working system. The meeting showed the use of the production environment prepared for the Karteco group installed on the IBM server at the School of Banking in Gdańsk and the appropriate training was carried out. Then it was shown how the working system can be used both for Karteco systems and for the design of the civic dust measuring network pm10 for the salon. This system is used in the construction of the citizen measuring network for Gdańsk.

![Karteco](https://gitlab.com/airq/website/-/blob/main/Images/karteco1.jpeg "Karteco2")
