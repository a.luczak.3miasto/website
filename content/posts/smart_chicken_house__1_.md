# Smart Chicken House

The main goal of the project is to support rural residents in monitoring the condition of the property where the animals live. The project involves the deployment of cameras on the property to monitor the movement of hens that are attacked by moving foxes. The design of the project indicates the need to use two cameras located in the place where the hens are. The signals from the cameras and the microphone next to the cameras are transmitted to the system that recognizes the image and voice. Then, these data are compared with the patterns, and when the image or signal matches the pattern, the signal is sent to the visualizer or loudspeaker. 

The system is based on communication using the GSM protocol and RPi microcomputers. 

Project implementation stages: 

Stage 1 - building a data processing environment using a Raspberry Pi microcontroller and creating a system based on Node-Red and Home Assistant for processing data coming from the camera. 

Stage 2 - the use of artificial intelligence algorithms for image and sound processing 

Stage 3 - building a mobile application for data identification 

