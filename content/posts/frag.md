# Project FRAG

This project aims to prepare and provide a map of Gdańsk together with placed marks of the air quality measurement modules allowing citizens of the Strzyża district verification of the quality of the air in those spots. Provided data should contain the current amount of particles within the air and the prognosis of those. Modules will be provided partially by the Airly company and by the JazzOps group itself. JazzOps modules are being created with help of a local electronics company: the SoftCad. Prognosis will be calculated by application prepared by the JazzOps group.  The Project Team consists of two developers and a leader, plus consultants: 3D printable objects designer. 

