# Project Radon

This project aims to prepare an IT system allowing for the measurement of the presence of radon in the air. The project involves the design and development of a system that allows remote monitoring of the radon level. The concentration of radon can be checked in the web browser and the mobile application from all sensors owned by the user. The solution should enable checking local radon values at any time by using the Radon Map. Data from all sensors will be collected on the server and analyzed, creating an overall map of Radon concentrations. The team also predicts interesting results after analysis of the data with the use of artificial intelligence. 

